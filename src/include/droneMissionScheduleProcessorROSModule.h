

#ifndef DRONE_MISSION_SCHEDULE_PROCESSOR_H
#define DRONE_MISSION_SCHEDULE_PROCESSOR_H

/// ROS ///

#include "ros/ros.h"

/// Math ///

#include <math.h>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <algorithm>
#include <array>
#include <fstream>


/// Drone Module ///

#include "droneModuleROS.h"

/// Trajectory Planner ///

#include "missionScheduling.h"
#include "spaceMap.h"
#include "observation3d.h"

/// Messages ///

#include "std_msgs/Bool.h"
#include <std_msgs/String.h>
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneHLCommand.h"
#include "droneMsgsROS/droneHLCommandAck.h"
#include "droneMsgsROS/dronePositionRefCommand.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "droneMsgsROS/droneMissionInfo.h"
#include "droneMsgsROS/droneGoTask.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/droneMissionPlannerCommand.h"
#include "droneMsgsROS/droneManagerStatus.h"
#include "droneMsgsROS/obstaclesTwoDim.h"
#include "droneMsgsROS/obsVector.h"
#include "droneMsgsROS/distancesToObstacles.h"

/// Services ///

#include "std_srvs/Empty.h"

/// Communications ///

#include "nodes_definition.h"

/// Frequency ///

#define FREQ_MISSION_PLANNER    10.0

float safetyDistanceParam;
float emergencyDistanceParam;
float droneRadiusParam;

class DroneMissionScheduler : public DroneModule
{	

private:
    //mission planner
    RobotMissionPlanner MissionScheduler;


    //counters
    int submissionCounter;
    int prevSubmission;
    int lastSubmission;
    int taskCounter;
    int prevTask;
    int counter;
    int flipCount;
    float h, k, rx, ry;
    double distanceToYaw;
    bool flagDistanceToYaw;

    //bool flagNewSubmission;
    //bool flagUpdateSubmissionTime;
    bool flagTaskRunning; //Volver a esta en el Mission Info
    bool flagFinishSubmissionLoop; //Volver a esta para acabar el Submision por el tiempo

    bool breakMission;
    bool objectOnFrame;
    bool haltMissionScheduler;
    bool emergencyCountVector;
    bool flagManagerInEmergency;

    std::string endVisualServoingArucoId;
    std::string emergencySoundFile;
    std::string missionXML;
    std::string cmdSent;
    std::vector<double> initialTargetSpeed;
    std::vector<bool> flipCountVector; //Boolean to see if flip was laready made
    std::vector<Observation3D> arucoObservations;
    std::vector< std::vector<double> > obstaclesMsgVector;
    droneMsgsROS::obstaclesTwoDim obstaclesMsg;
    std::ofstream myfile;



private:
    //Read Parameters from File
    void getParametersFromFile();

    //drone pose (Subscriber)
    std::string droneEstimatedPoseTopicName;
    droneMsgsROS::dronePose droneEstimatedPoseMsg;
    ros::Subscriber dronePoseSub;
    void dronePoseCallback(const droneMsgsROS::dronePose::ConstPtr& msg);

    //drone speeds (Subscriber)
    std::string droneEstimatedSpeedsTopicName;
    droneMsgsROS::droneSpeeds droneEstimatedSpeedsMsg;
    ros::Subscriber droneSpeedsSub;
    void droneSpeedsCallback(const droneMsgsROS::droneSpeeds::ConstPtr& msg);

    //drone yaw command (Subscriber)
    std::string droneYawRefCommandTopicName;
    droneMsgsROS::droneYawRefCommand droneYawRefCommandMsg;
    ros::Subscriber droneYawRefCommandSub;
    void droneYawRefCommandCallback(const droneMsgsROS::droneYawRefCommand::ConstPtr& msg);

    //VS ROI Ack (Subscriber)
    std::string droneROIAckCommandTopicName;
    ros::Subscriber droneROIAckCommandSub;
    void droneROIAckCommandCallback(const std_msgs::Bool::ConstPtr& msg);

    //Manager Status (Subscriber)
    std::string droneManagerStatusTopicName;
    int32_t droneManagerStatusMsg;
    ros::Subscriber droneManagerStatusSub;
    void droneManagerStatusCallback(const droneMsgsROS::droneManagerStatus::ConstPtr &msg);

    //go to task (Subscriber)
    std::string droneGoTaskTopicName;
    droneMsgsROS::droneGoTask droneGoTaskMsg;
    ros::Subscriber droneGoTaskSub;
    void droneGoTaskCallback(const droneMsgsROS::droneGoTask::ConstPtr& msg);

    //drone HL command ack (Subscriber)
    std::string droneHLCommAckTopicName;
    ros::Subscriber droneHLCommAckSub;
    void droneHLCommandAckCallback(const droneMsgsROS::droneHLCommandAck::ConstPtr& msg);

    //Obstacles (Subscriber)
    std::string droneObstaclesTopicName;
    ros::Subscriber droneObstaclesSub;
    void droneObstaclesCallback(const droneMsgsROS::obstaclesTwoDim::ConstPtr& msg);

    //Distance to Obstacles (Subscriber)
    std::string droneDistanceToObstaclesTopicName;
    ros::Subscriber droneDistanceToObstaclesSub;
    void droneDistanceToObstaclesCallback(const droneMsgsROS::distancesToObstacles::ConstPtr& msg);

    //AruCo List (Subscriber)
    std::string droneObsVectorTopicName;
    ros::Subscriber droneObsVectorSub;
    void droneObsVectorCallback(const droneMsgsROS::obsVector::ConstPtr& msg);

    //drone Mission point (Publisher)
    std::string dronePositionRefCommandTopicName;
    droneMsgsROS::dronePositionRefCommand dronePositionRefCommandMsg;
    ros::Publisher dronePositionRefCommandPub;
    int publishPositionRefCommand(droneMsgsROS::dronePositionRefCommand dronePositionRefCommandIn);

    //drone Speed Reference (Publisher)
    std::string droneSpeedsRefCommandTopicName;
    droneMsgsROS::droneSpeeds droneSpeedsRefCommandMsg;
    ros::Publisher droneSpeedsRefCommandPub;
    int publishSpeedsRefCommand(droneMsgsROS::droneSpeeds droneSpeedsRefCommandIn);

    //yaw to look (Publisher)
    std::string droneYawToLookTopicName;
    droneMsgsROS::droneYawRefCommand droneYawToLookMsg;
    ros::Publisher droneYawToLookPub;
    int publishYawToLook(droneMsgsROS::droneYawRefCommand droneYawToLookIn);

    //point to look (Publisher)
    std::string dronePointToLookTopicName;
    droneMsgsROS::dronePositionRefCommand dronePointToLookMsg;
    ros::Publisher dronePointToLookPub;
    int publishPointToLook(droneMsgsROS::dronePositionRefCommand dronePointToLookIn);

    //drone mission info (Publisher)
    std::string droneMissionInfoTopicName;
    droneMsgsROS::droneMissionInfo droneMissionInfoMsg;
    ros::Publisher droneMissionInfoPub;
    int publishMissionInfo(droneMsgsROS::droneMissionInfo droneMissionInfoIn);

    //Mission Planner Command (Publisher)
    std::string droneMissionPlannerCommandTopicName;
    droneMsgsROS::droneMissionPlannerCommand droneMissionPlannerCommandMsg;
    ros::Publisher droneMissionPlannerCommandPub;
    int publishMissionPlannerCommand(droneMsgsROS::droneMissionPlannerCommand droneMissionPlannerCommandMsgIn);

    //Mission State (Publisher)
    std::string droneMissionStateTopicName;
    std_msgs::Bool droneMissionStateMsg;
    ros::Publisher droneMissionStatePub;
    int publishMissionState(std_msgs::Bool missionStateMsgIn);

    //Speech Command (Publisher)
    std::string droneSpeechCommandTopicName;
    std_msgs::String droneSpeechCommandMsg;
    ros::Publisher droneSpeechCommandPub;
    int publishSpeechCommand(std_msgs::String droneSpeechCommandMsgIn);

    //Sound Command (Publisher)
    std::string droneFileToReproduceTopicName;
    std_msgs::String droneFileToReproduceCommandMsg;
    ros::Publisher droneFileToReproduceCommandPub;
    int publishFileToReproduceCommand(std_msgs::String droneFileToReproduceCommandMsgIn);



/// Others ///

protected:
    ros::Time startMissionTime;
    ros::Time submissionInitTime;
    ros::Time taskInitTime;
    ros::Time suspendedInitTime;
    ros::Duration supendedTime;

    bool init();
    bool resetValues();

public:
    DroneMissionScheduler();
    ~DroneMissionScheduler();
	
    void open(ros::NodeHandle & nIn, std::string moduleName);
	void close();

    bool runStart();
    bool readNewTask();
    bool sendNewTask();
    bool sendMissionInfo();
    bool sendCommandValues();
    bool sendTaskEnded();
    bool sendTaskReady();
    bool sendMissionMonitor();
    bool calculatePointDistance();
    bool calculateSpeedDistance();
    bool calculateTaskTimeDuration();
    bool calculateBatteryDuration();
    bool calculateSubmissionTimeDuration();
    bool calculateMissionPointDistanceToObstacle(std::vector<double> missionPointIn);
    bool monitorVS();
    bool monitorTakingOff();
    bool monitorFlip();
    bool moveEmergency();

};


#endif
