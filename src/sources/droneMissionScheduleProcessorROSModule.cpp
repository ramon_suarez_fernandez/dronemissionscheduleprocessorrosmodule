#include "droneMissionScheduleProcessorROSModule.h"


using namespace std;


DroneMissionScheduler::DroneMissionScheduler() : DroneModule(droneModule::active,FREQ_MISSION_PLANNER) // See what DroneModule does here //
{
    if(!init())
        cout<<"Error in init"<<endl;

    return;
}


DroneMissionScheduler::~DroneMissionScheduler()
{
	close();
	return;
}


void DroneMissionScheduler::getParametersFromFile()
{
    /// SUBSCRIBERS ///
    ros::param::get("~end_visual_servoing_aruco_id", endVisualServoingArucoId);
    if ( endVisualServoingArucoId.length() == 0)
    {
        endVisualServoingArucoId = "5";
    }

    ros::param::get("~emergency_sound_file_to_play", emergencySoundFile);
    if ( emergencySoundFile.length() == 0)
    {
        emergencySoundFile = "woop_woop.mp3";
    }

    ros::param::get("~mission_config_file", missionXML);
    if ( missionXML.length() == 0)
    {
        missionXML = "missionSchedule.xml";
    }

    ros::param::get("~emergency_distance_param", emergencyDistanceParam);
    if (!emergencyDistanceParam)
    {
        emergencyDistanceParam = 0.10;
    }

    ros::param::get("~safety_distance_param", safetyDistanceParam);
    if (!safetyDistanceParam)
    {
        safetyDistanceParam = 0.20;
    }

    ros::param::get("~drone_radius_param", droneRadiusParam);
    if (!droneRadiusParam)
    {
        droneRadiusParam = 0.30;
    }

    ros::param::get("~drone_Speech_Command_Topic_Name", droneSpeechCommandTopicName);
    if ( droneSpeechCommandTopicName.length() == 0)
    {
        droneSpeechCommandTopicName = "message_to_say";
    }
    std::cout << "drone_Speech_Command_Topic_Name=" << droneSpeechCommandTopicName <<std::endl;

    ros::param::get("~drone_Estimated_Pose_Topic_Name", droneEstimatedPoseTopicName);
    if ( droneEstimatedPoseTopicName.length() == 0)
    {
        //droneEstimatedPoseTopicName = "ArucoSlam_EstimatedPose";
        droneEstimatedPoseTopicName = "EstimatedPose_droneGMR_wrt_GFF";
    }
    std::cout << "drone_Estimated_Pose_Topic_Name=" << droneEstimatedPoseTopicName <<std::endl;

    ros::param::get("~drone_Estimated_Speeds_Topic_Name", droneEstimatedSpeedsTopicName);
    if ( droneEstimatedSpeedsTopicName.length() == 0)
    {
        droneEstimatedSpeedsTopicName="trajectoryControllerSpeedReferencesRebroadcast";
    }
    std::cout << "drone_Estimated_Speeds_Topic_Name=" << droneEstimatedSpeedsTopicName << std::endl;

    ros::param::get("~drone_HL_Comm_Ack_Topic_Name", droneHLCommAckTopicName);
    if ( droneHLCommAckTopicName.length() == 0)
    {
        droneHLCommAckTopicName="droneMissionHLCommandAck";
    }
    std::cout << "drone_HL_Comm_Ack_Topic_Name=" << droneHLCommAckTopicName << std::endl;

    ros::param::get("~drone_Go_Task_Topic_Name", droneGoTaskTopicName);
    if (droneGoTaskTopicName.length() == 0)
    {
        droneGoTaskTopicName="droneGoTask";
    }
    std::cout << "drone_Go_Task_Topic_Name=" << droneGoTaskTopicName << std::endl;

    ros::param::get("~drone_YawRef_Command_Topic_Name", droneYawRefCommandTopicName);
    if ( droneYawRefCommandTopicName.length() == 0)
    {
        droneYawRefCommandTopicName="droneControllerYawRefCommand";
    }
    std::cout << "drone_YawRef_Command_Topic_Name=" << droneYawRefCommandTopicName << std::endl;

    ros::param::get("~tracking_object_topic_name", droneROIAckCommandTopicName);
    if ( droneROIAckCommandTopicName.length() == 0)
    {
        droneROIAckCommandTopicName="tracking_object";
    }
    std::cout << "tracking_object_topic_name=" << droneROIAckCommandTopicName << std::endl;

    ros::param::get("~drone_Manager_Status_Topic_Name", droneManagerStatusTopicName);
    if ( droneManagerStatusTopicName.length() == 0)
    {
        droneManagerStatusTopicName="droneManagerStatus";
    }
    std::cout << "drone_Manager_Status_Topic_Name=" << droneManagerStatusTopicName <<std::endl;

    ros::param::get("~drone_Obstacles_Topic_Name", droneObstaclesTopicName);
    if ( droneObstaclesTopicName.length() == 0)
    {
        droneObstaclesTopicName="obstacles";
    }
    std::cout << "drone_Obstacles_Topic_Name=" << droneObstaclesTopicName << std::endl;

    ros::param::get("~drone_Distance_to_Obstacles_Topic_Name", droneDistanceToObstaclesTopicName);
    if ( droneDistanceToObstaclesTopicName.length() == 0)
    {
        droneDistanceToObstaclesTopicName="distanceToObstacles";
    }
    std::cout << "drone_Distance_to_Obstacles_Topic_Name=" << droneDistanceToObstaclesTopicName << std::endl;

    ros::param::get("~drone_Obs_Vector_Topic_Name", droneObsVectorTopicName);
    if ( droneObsVectorTopicName.length() == 0)
    {
        droneObsVectorTopicName="arucoObservation";
    }
    std::cout << "drone_Obs_Vector_Topic_Name=" << droneObsVectorTopicName << std::endl;


    /// PUBLISHERS ///

    ros::param::get("~drone_Mission_State_Topic_Name", droneMissionStateTopicName);
    if ( droneMissionStateTopicName.length() == 0)
    {
        droneMissionStateTopicName="missionState";
    }
    std::cout << "drone_Mission_State_Topic_Name=" << droneMissionStateTopicName << std::endl;

    ros::param::get("~mission_Planner_Command_Topic_Name", droneMissionPlannerCommandTopicName);
    if (droneMissionPlannerCommandTopicName.length() == 0)
    {
        droneMissionPlannerCommandTopicName="droneMissionPlannerCommand";
    }
    std::cout << "mission_Planner_Command_Topic_Name=" << droneMissionPlannerCommandTopicName << std::endl;

    ros::param::get("~drone_Position_Ref_Command_Topic_Name", dronePositionRefCommandTopicName);
    if ( dronePositionRefCommandTopicName.length() == 0)
    {
        dronePositionRefCommandTopicName="droneMissionPoint";
    }
    std::cout << "drone_Position_Ref_Command_Topic_Name=" << dronePositionRefCommandTopicName << std::endl;

    ros::param::get("~drone_Yaw_To_Look_Topic_Name", droneYawToLookTopicName);
    if ( droneYawToLookTopicName.length() == 0)
    {
        droneYawToLookTopicName="droneYawToLook";
    }
    std::cout << "drone_Yaw_To_Look_Topic_Name=" << droneYawToLookTopicName << std::endl;

    ros::param::get("~drone_Point_To_Look_Topic_Name", dronePointToLookTopicName);
    if ( dronePointToLookTopicName.length() == 0)
    {
        dronePointToLookTopicName="dronePointToLook";
    }
    std::cout << "drone_Point_To_Look_Topic_Name=" << dronePointToLookTopicName <<std::endl;

    ros::param::get("~drone_Mission_Info_Topic_Name", droneMissionInfoTopicName);
    if ( droneMissionInfoTopicName.length() == 0)
    {
        droneMissionInfoTopicName="droneMissionInfo";
    }
    std::cout << "drone_Mission_Info_Topic_Name=" << droneMissionInfoTopicName << std::endl;

    ros::param::get("~drone_Speeds_Ref_Command_Topic_Name", droneSpeedsRefCommandTopicName);
    if ( droneSpeedsRefCommandTopicName.length() == 0)
    {
        droneSpeedsRefCommandTopicName="droneSpeedsRefs";
    }
    std::cout << "drone_Speeds_Ref_Command_Topic_Name=" << droneSpeedsRefCommandTopicName << std::endl;

    ros::param::get("~drone_Speeds_Ref_Command_Topic_Name", droneSpeedsRefCommandTopicName);
    if ( droneSpeedsRefCommandTopicName.length() == 0)
    {
        droneSpeedsRefCommandTopicName="droneSpeedsRefs";
    }
    std::cout << "drone_Speeds_Ref_Command_Topic_Name=" << droneSpeedsRefCommandTopicName << std::endl;

    ros::param::get("~file_to_reproduce_topic_name", droneFileToReproduceTopicName);
    if ( droneFileToReproduceTopicName.length() == 0)
    {
        droneFileToReproduceTopicName="file_to_reproduce";
    }
    std::cout<<"file_to_reproduce_topic_name="<<droneFileToReproduceTopicName<<std::endl;

}


void DroneMissionScheduler::open(ros::NodeHandle & nIn, std::string moduleName)
{

	//Node
    DroneModule::open(nIn,moduleName);

    /// Read from File ///

    DroneMissionScheduler::getParametersFromFile();

    //Set mission
    MissionScheduler.setMission(stackPath+"configs/drone"+stringId+"/"+missionXML); /// PONER XML EN LAUNCHER ///

    //Init mission
    submissionCounter=MissionScheduler.submissionTree.at(0);
    taskCounter=MissionScheduler.taskTree.at(0).at(0);


    //Init Aruco flip status
    for (int i = 0; i < MissionScheduler.arucoIdVector.size();i++)
    {
        //cout << "arucoId: " << MissionScheduler.arucoIdVector.at(i) << endl;
        flipCountVector.push_back(false);
        //cout << "arucoIdFlipBool: " << flipCountVector.at(i) << endl;

    }


    cout<<"\n----------INITIAL SUBMISSION: "<<submissionCounter<<"----------"<<endl;
    cout<<"\n----------INITIAL TASK: "<<taskCounter<<"----------"<<endl;


    /////////////////// TOPICS ///////////////////

    /// Subscribers ///

    // Pose Estimated //
    // rostopic pub -1 /drone0/ArucoSlam_EstimatedPose droneMsgsROS/dronePose -- 123 0.0 0.0 1.5 0.0 0.0 0.0 "a" "a" "a"
    dronePoseSub = n.subscribe(droneEstimatedPoseTopicName, 1, &DroneMissionScheduler::dronePoseCallback, this);

    // Speeds Estimated //
    // rostopic pub -1 /drone0/ground_speed droneMsgsROS/droneSpeeds -- 123 0.0 0.0 1.5 0.0 0.0 0.0
    droneSpeedsSub = n.subscribe(droneEstimatedSpeedsTopicName, 1, &DroneMissionScheduler::droneSpeedsCallback, this);

    // HL Command Acknowledgement //
    // rostopic pub -1 /drone0/droneMissionHLCommandAck droneMsgsROS/droneHLCommandAck -- 123 true
    droneHLCommAckSub = n.subscribe(droneHLCommAckTopicName, 1, &DroneMissionScheduler::droneHLCommandAckCallback, this);

    // Go to Task //
    droneGoTaskSub = n.subscribe(droneGoTaskTopicName, 1, &DroneMissionScheduler::droneGoTaskCallback, this);

    // Yaw Reference //
    droneYawRefCommandSub = n.subscribe(droneYawRefCommandTopicName, 1, &DroneMissionScheduler::droneYawRefCommandCallback, this);

    // VS ROI in Image //
    droneROIAckCommandSub = n.subscribe(droneROIAckCommandTopicName, 1, &DroneMissionScheduler::droneROIAckCommandCallback, this);

    // Manager State Subsriber //
    droneManagerStatusSub = n.subscribe(droneManagerStatusTopicName, 1, &DroneMissionScheduler::droneManagerStatusCallback, this);

    // Obstacles //
    droneObstaclesSub = n.subscribe(droneObstaclesTopicName, 1, &DroneMissionScheduler::droneObstaclesCallback, this);

    // Obstacles Distance to Drone //
    droneDistanceToObstaclesSub = n.subscribe(droneDistanceToObstaclesTopicName, 1, &DroneMissionScheduler::droneDistanceToObstaclesCallback, this);

    // AruCo Observations //
    droneObsVectorSub = n.subscribe(droneObsVectorTopicName, 1, &DroneMissionScheduler::droneObsVectorCallback, this);

    // Error Message //
    //droneErrorCommandSub = n.subscribe(, 1, &DroneMissionScheduler::, this);


    /// Publishers ///

    // Mission State //
    droneMissionStatePub=n.advertise<std_msgs::Bool>(droneMissionStateTopicName, 1, true);

    // HL Command //
    droneMissionPlannerCommandPub=n.advertise<droneMsgsROS::droneMissionPlannerCommand>(droneMissionPlannerCommandTopicName, 1, true);

    // Point to Reach //
    dronePositionRefCommandPub=n.advertise<droneMsgsROS::dronePositionRefCommand>(dronePositionRefCommandTopicName, 1, true);

    // Yaw to Look //
    droneYawToLookPub=n.advertise<droneMsgsROS::droneYawRefCommand>(droneYawToLookTopicName, 1, true);

    // Point to Look //
    dronePointToLookPub=n.advertise<droneMsgsROS::dronePositionRefCommand>(dronePointToLookTopicName, 1, true);

    // Mission Info //
    droneMissionInfoPub=n.advertise<droneMsgsROS::droneMissionInfo>(droneMissionInfoTopicName, 1, true);

    //Speed Reference Command //
    droneSpeedsRefCommandPub=n.advertise<droneMsgsROS::droneSpeeds>(droneSpeedsRefCommandTopicName, 1, true);

    //Speech Command //
    droneSpeechCommandPub=n.advertise<std_msgs::String>(droneSpeechCommandTopicName, 1, true);

    //Sound Command //
    droneFileToReproduceCommandPub=n.advertise<std_msgs::String>(droneFileToReproduceTopicName, 1, true);

    // Flag of module opened //
    droneModuleOpened=true;

    droneMissionStateMsg.data = false;
    publishMissionState(droneMissionStateMsg);

    /// WAIT FOR START SERVICE ///
    while(!runStart())
    {
        ros::spinOnce();
    }

    /// SEND FIRST TASKS ///
    readNewTask();
    sendNewTask();

	return;
}

/// Subscriber Callbacks ///


void DroneMissionScheduler::droneObsVectorCallback(const droneMsgsROS::obsVector::ConstPtr &msg) /// ARUCO OBSERVATIONS! ///
{
    arucoObservations.clear();

    for(unsigned int i=0; i<msg->obs.size();i++)
    {
        droneMsgsROS::Observation3D obs_msg = msg->obs[i];
        Observation3D observation;

        observation.id = obs_msg.id;
        arucoObservations.push_back(observation);


        for (unsigned int j=0; j<MissionScheduler.arucoIdVector.size();j++)
        {
            if(obs_msg.id == MissionScheduler.arucoIdVector.at(j) && !flipCountVector.at(j))
            {

                    prevSubmission = submissionCounter;
                    prevTask = taskCounter-1;
                    for(unsigned int iSubm=0;iSubm<MissionScheduler.submissionTree.size();iSubm++)
                    {
                        if(MissionScheduler.submissionTreeDescription.at(iSubm)=="XXXXX FLIP XXXXX" && MissionScheduler.arucoIdVector.at(iSubm) == obs_msg.id)
                        {
                            droneFileToReproduceCommandMsg.data = emergencySoundFile;
                            droneFileToReproduceCommandPub.publish(droneFileToReproduceCommandMsg);

                            // Reset Flags to initial //
                            resetValues();
                            breakMission = true;

                            submissionCounter = MissionScheduler.submissionTree.at(iSubm);
                            taskCounter = MissionScheduler.taskTree.at(iSubm).at(0);

                            DroneMissionScheduler::readNewTask();
                            DroneMissionScheduler::sendNewTask();
                            cout << "\033[1;31m/////////////FLIP FOR ARUCO " << MissionScheduler.arucoIdVector.at(iSubm) << " MADE/////////////\033[0m" << endl;
                            break;
                        }
                    }
                    flipCountVector.at(j)=true;

                return;
            }
        }

    }

    return;
}


bool IsEmergency (float i) {

    return ((i<(emergencyDistanceParam+droneRadiusParam)));
}


bool IsSafety (float i) {

    return ((i<(safetyDistanceParam+droneRadiusParam)));
}


void DroneMissionScheduler::droneDistanceToObstaclesCallback(const droneMsgsROS::distancesToObstacles::ConstPtr &msg)
{

    std::vector<float> obsDistances;
    std::vector<int> obsDistancesIds;

    for (unsigned int i=0; i<msg->distances_to_obstacles.size(); i++)
    {
        obsDistances.push_back(msg->distances_to_obstacles.at(i).distance_to_obstacle);
        obsDistancesIds.push_back(msg->distances_to_obstacles.at(i).id_obstacle);
    }

    std::vector<float>::iterator it = std::find_if (obsDistances.begin(), obsDistances.end(), IsEmergency);

    if (it!= obsDistances.end())
    {
        float obsDistanceEmergency = *it;
        int obsIdEmergency = obsDistancesIds.at(it - obsDistances.begin());

        cout << "\033[1;31m/////////////EMERGENCY DISTANCE REACHED " << obsDistanceEmergency << " TO OBSTACLE " << obsIdEmergency << " /////////////\033[0m" << endl;
        //droneSpeechCommandMsg.data = "DRON MUY CERCA DE OBSTACULO MISION PARADA!";
        //droneSpeechCommandPub.publish(droneSpeechCommandMsg);

//        if(!emergencyCountVector)
//        {
//                prevSubmission = submissionCounter;
//                prevTask = taskCounter;true

//                for(unsigned int iSubm=0;iSubm<MissionScheduler.submissionTree.size();iSubm++)
//                {
//                    if(MissionScheduler.submissionTreeDescription.at(iSubm)=="XXXXX GO SAFE POINT XXXXX")
//                    {
//                        // Reset Flags to initial //
//                        resetValues();
//                        breakMission = true;

//                        submissionCounter = MissionScheduler.submissionTree.at(iSubm);
//                        taskCounter = MissionScheduler.taskTree.at(iSubm).at(0);

//                        DroneMissionScheduler::readNewTask();
//                        DroneMissionScheduler::sendNewTask();
//                        cout << "\033[1;31m///////////// EMERGENCY MOVE BEGIN! /////////////\033[0m" << endl;
//                        break;
//                    }
//                }
//                emergencyCountVector=true;

//            return;
//        }


    }

    std::vector<float>::iterator its = std::find_if (obsDistances.begin(), obsDistances.end(), IsSafety);

    if (its!= obsDistances.end())
    {
        float obsDistanceSafety = *its;
        int obsIdSafety = obsDistancesIds.at(its - obsDistances.begin());

        cout << "\033[1;31m/////////////SAFETY DISTANCE REACHED " << obsDistanceSafety << " TO OBSTACLE " << obsIdSafety << " STOP COURSE/////////////\033[0m" << endl;
        //droneFileToReproduceCommandMsg.data = emergencySoundFile;
        //droneFileToReproduceCommandPub.publish(droneFileToReproduceCommandMsg);


    }

    return;
}


void DroneMissionScheduler::droneObstaclesCallback(const droneMsgsROS::obstaclesTwoDim::ConstPtr &msg)
{

    //Set


    obstaclesMsg.walls=msg->walls;
    obstaclesMsg.poles=msg->poles;

    //Aux vars
    unsigned int idObstacle;
    double yawAngleIn;
    std::vector<double> centerPointIn(2);
    std::vector<double> radiusIn(2);
    std::vector<double> sizeIn(2);


    std::vector<RectangleObstacle2d> RectanglesIn;
    for(unsigned int i=0;i<obstaclesMsg.walls.size();i++)
    {
        RectangleObstacle2d OneRectangle;
        idObstacle=obstaclesMsg.walls[i].id;
        centerPointIn[0]=obstaclesMsg.walls[i].centerX;
        centerPointIn[1]=obstaclesMsg.walls[i].centerY;
        sizeIn[0]=obstaclesMsg.walls[i].sizeX;
        sizeIn[0]=obstaclesMsg.walls[i].sizeY;
        yawAngleIn=obstaclesMsg.walls[i].yawAngle;

        OneRectangle.define(idObstacle,centerPointIn,sizeIn,yawAngleIn);

        RectanglesIn.push_back(OneRectangle);
    }


    return;
}


void DroneMissionScheduler::dronePoseCallback(const droneMsgsROS::dronePose::ConstPtr& msg)
{

    droneEstimatedPoseMsg.time=msg->time;
    droneEstimatedPoseMsg.x=msg->x;
    droneEstimatedPoseMsg.y=msg->y;
    droneEstimatedPoseMsg.z=msg->z;
    droneEstimatedPoseMsg.yaw=msg->yaw;
    droneEstimatedPoseMsg.pitch=msg->pitch;
    droneEstimatedPoseMsg.roll=msg->roll;





    return;
}

void DroneMissionScheduler::droneROIAckCommandCallback(const std_msgs::Bool::ConstPtr &msg)
{

    objectOnFrame = msg->data;
}

void DroneMissionScheduler::droneManagerStatusCallback(const droneMsgsROS::droneManagerStatus::ConstPtr &msg)
{

    droneManagerStatusMsg = msg->status;
    switch(droneManagerStatusMsg)
    {
        case droneMsgsROS::droneManagerStatus::TAKINGOFF:
            cout << "Manager Status Taking Off" << endl;
            break;

        case droneMsgsROS::droneManagerStatus::HOVERING:
            haltMissionScheduler = false;
            cout << "Manager Status HOVERING" << endl;
            break;

        case droneMsgsROS::droneManagerStatus::HOVERING_VISUAL_SERVOING:
            cout << "Manager Status HOVERING_VISUAL_SERVOING" << endl;
            break;

        case droneMsgsROS::droneManagerStatus::LANDED:
            cout << "Manager Status LANDED" << endl;
            break;

        case droneMsgsROS::droneManagerStatus::LANDING:
            cout << "Manager Status LANDING" << endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD:
            {
                //ros::Duration taskDuration;
                //ros::Duration missionPlannerHaltInitTime;
                haltMissionScheduler = true;
                cout << "Manager Status MOVING_MANUAL_ALTITUD Halt Mission Planner" << endl;

                while(haltMissionScheduler == true)
                {

                    //taskDuration=ros::Time::now()-missionPlannerHaltInitTime;
                    //cout << "\033[1;34m........Task Elapsed Time: \033[0m" << "\033[1;32m" << taskDuration.toSec() <<"\033[0m"<< endl;

                    ros::Duration(1).sleep();
                    ros::spinOnce();
                }

                //readNewTask();
                //sendNewTask();

                break;
            }

        case droneMsgsROS::droneManagerStatus::MOVING_MANUAL_THRUST:
            {
                //ros::Duration taskDuration;
                //ros::Duration missionPlannerHaltInitTime;
                haltMissionScheduler = true;
                cout << "Manager Status MOVING_MANUAL_THRUST Halt Mission Planner" << endl;

                while(haltMissionScheduler == true)
                {

                    //taskDuration=ros::Time::now()-missionPlannerHaltInitTime;
                    //cout << "\033[1;34m........Task Elapsed Time: \033[0m" << "\033[1;32m" << taskDuration.toSec() <<"\033[0m"<< endl;

                    ros::Duration(1).sleep();
                    ros::spinOnce();
                }

                //readNewTask();
                //sendNewTask();

                break;
            }

        case droneMsgsROS::droneManagerStatus::MOVING_POSITION:
            cout << "Manager Status MOVING_POSITION" << endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_FLIP_RIGHT:
            cout << "Manager Status MOVING_FLIP_RIGHT" << endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_FLIP_LEFT:
            cout << "Manager Status MOVING_FLIP_LEFT" << endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_FLIP_FRONT:
            cout << "Manager Status MOVING_FLIP_FRONT" << endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_FLIP:
            cout << "Manager Status MOVING_FLIP" << endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_FLIP_BACK:
            cout << "Manager Status MOVING_FLIP_BACK" << endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_SPEED:
            cout << "Manager Status MOVING_SPEED" << endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_TRAJECTORY:
            cout << "Manager Status MOVING_TRAJECTORY" << endl;
            break;
        case droneMsgsROS::droneManagerStatus::MOVING_EMERGENCY:
            cout << "Manager Status MOVING_TRAJECTORY" << endl;
            break;
        case droneMsgsROS::droneManagerStatus::MOVING_VISUAL_SERVOING:
            cout << "Manager Status MOVING_VISUAL_SERVOING" << endl;
            break;

        case droneMsgsROS::droneManagerStatus::SLEEPING:
            cout << "Manager Status SLEEPING" << endl;
            break;

        case droneMsgsROS::droneManagerStatus::EMERGENCY:
            {
                cout << "Manager Status EMERGENCY" << endl;
                flagManagerInEmergency = true;
                breakMission = true;

                prevSubmission = submissionCounter;
                prevTask = taskCounter;


                break;

            }

        case droneMsgsROS::droneManagerStatus::UNKNOWN:
            cout << "Manager Status UNKNOWN" << endl;
            break;

    }

    if (droneManagerStatusMsg != droneMsgsROS::droneManagerStatus::EMERGENCY && flagManagerInEmergency)
    {
        resetValues();
        breakMission = true;

        submissionCounter = prevSubmission;
        taskCounter = prevTask;

        DroneMissionScheduler::readNewTask();
        DroneMissionScheduler::sendNewTask();

    }
}

void DroneMissionScheduler::droneSpeedsCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg)
{
    droneEstimatedSpeedsMsg.time = msg->time;
    droneEstimatedSpeedsMsg.dx = msg->dx;
    droneEstimatedSpeedsMsg.dy = msg->dy;
    droneEstimatedSpeedsMsg.dz = msg->dz;
    droneEstimatedSpeedsMsg.dyaw = msg->dyaw;
    droneEstimatedSpeedsMsg.dpitch = msg->dpitch;
    droneEstimatedSpeedsMsg.droll = msg->droll;

}

void DroneMissionScheduler::droneYawRefCommandCallback(const droneMsgsROS::droneYawRefCommand::ConstPtr& msg)
{

    droneYawRefCommandMsg.header=msg->header;
    droneYawRefCommandMsg.yaw=msg->yaw;


    //distanceToYaw=fabs(droneEstimatedPoseMsg.yaw-droneYawRefCommandMsg.yaw);
    flagDistanceToYaw=true;

    //std::cout<<"droneYawRefCommandMsg.yaw="<<droneYawRefCommandMsg.yaw<<std::endl;
    //std::cout<<"droneYawRefCommandMsg.header.frame_id="<<droneYawRefCommandMsg.header.frame_id<<std::endl;

    return;
}

void DroneMissionScheduler::droneHLCommandAckCallback(const droneMsgsROS::droneHLCommandAck::ConstPtr& msg)
{

    if(msg->ack == true || MissionScheduler.TheTask.type == missionSchedulerType::sleep)
    {
        cout << "recibo ack" << endl;

        DroneMissionScheduler::sendCommandValues();
        DroneMissionScheduler::sendTaskReady();
        DroneMissionScheduler::sendMissionInfo();

        ros::spinOnce();
        ros::Duration(1.0).sleep();
        ros::spinOnce();


        while(!sendMissionMonitor() && !breakMission && ros::ok())
        {

           //ROS_INFO("Processing Mission!");
           ros::spinOnce();
           ros::Duration(0.05).sleep();

        }
        if (!breakMission)
        {
            if(DroneMissionScheduler::sendTaskEnded())
            {
                DroneMissionScheduler::readNewTask();
                DroneMissionScheduler::sendNewTask();

            }
        }
        breakMission = false;
    }
    else
    {
        ROS_INFO("Oooops, False ack");

    }

    return;
}

void DroneMissionScheduler::droneGoTaskCallback(const droneMsgsROS::droneGoTask::ConstPtr& msg)
{
    // Read Go to Task //
    droneGoTaskMsg.time=msg->time;
    droneGoTaskMsg.submissionId=msg->submissionId;
    droneGoTaskMsg.taskId=msg->taskId;

    // Reset Flags to initial //
    resetValues();
    breakMission = true;


    // Set Submission and Task Defined by User //
    submissionCounter=droneGoTaskMsg.submissionId;
    taskCounter=droneGoTaskMsg.taskId;

    DroneMissionScheduler::readNewTask();
    DroneMissionScheduler::sendNewTask();
    //end
    return;
}


/// Publishers ///

int DroneMissionScheduler::publishMissionPlannerCommand(droneMsgsROS::droneMissionPlannerCommand droneMissionPlannerCommandMsgIn)
{
    if(droneModuleOpened==false)
        return 0;

    droneMissionPlannerCommandPub.publish(droneMissionPlannerCommandMsgIn);

    return 1;
}

int DroneMissionScheduler::publishMissionState(std_msgs::Bool missionStateMsgIn)
{
    if(droneModuleOpened==false)
        return 0;

    droneMissionStatePub.publish(missionStateMsgIn);

    return 1;
}

int DroneMissionScheduler::publishPositionRefCommand(droneMsgsROS::dronePositionRefCommand dronePositionRefCommandIn)
{
    if(droneModuleOpened==false)
        return 0;

    dronePositionRefCommandPub.publish(dronePositionRefCommandIn);

    return 1;
}

int DroneMissionScheduler::publishSpeedsRefCommand(droneMsgsROS::droneSpeeds droneSpeedsRefCommandIn)
{
    if(droneModuleOpened==false)
        return 0;

    droneSpeedsRefCommandPub.publish(droneSpeedsRefCommandIn);

    return 1;
}

int DroneMissionScheduler::publishYawToLook(droneMsgsROS::droneYawRefCommand droneYawToLookIn)
{
    if(droneModuleOpened==false)
        return 0;

    droneYawToLookPub.publish(droneYawToLookIn);

    return 1;
}

int DroneMissionScheduler::publishPointToLook(droneMsgsROS::dronePositionRefCommand dronePointToLookIn)
{
    if(droneModuleOpened==false)
        return 0;

    dronePointToLookPub.publish(dronePointToLookIn);

    return 1;
}

int DroneMissionScheduler::publishMissionInfo(droneMsgsROS::droneMissionInfo droneMissionInfoIn)
{
    if(droneModuleOpened==false)
        return 0;

    droneMissionInfoPub.publish(droneMissionInfoIn);

    return 1;
}

int DroneMissionScheduler::publishSpeechCommand(std_msgs::String droneSpeechCommandMsgIn)
{
    if(droneModuleOpened==false)
        return 0;

    droneSpeechCommandPub.publish(droneSpeechCommandMsgIn);

    return 1;
}

int DroneMissionScheduler::publishFileToReproduceCommand(std_msgs::String droneFileToReproduceCommandMsgIn)
{
    if(droneModuleOpened==false)
        return 0;

    droneFileToReproduceCommandPub.publish(droneFileToReproduceCommandMsgIn);

    return 1;
}



/// Functions ///

bool DroneMissionScheduler::init()
{
    //counters
    submissionCounter=0;
    taskCounter=0;
    counter=0;
    flipCount = 0;

    flagTaskRunning=false;
    flagFinishSubmissionLoop=false;
    flagManagerInEmergency = false;

    breakMission = false;
    objectOnFrame==false;
    //flagNewSubmission=true;
    //flagUpdateSubmissionTime=true;


    flagDistanceToYaw=false;

    return true;
}

void DroneMissionScheduler::close()
{

    DroneModule::close();
	return;
}

bool DroneMissionScheduler::resetValues()
{
    //Init mission
    //startMissionTime=ros::Time::now();
    submissionInitTime=ros::Time::now();
    taskInitTime==ros::Time::now();

    submissionCounter=MissionScheduler.submissionTree.at(0);
    taskCounter=MissionScheduler.taskTree.at(0).at(0);

    flagTaskRunning=false;
    flagFinishSubmissionLoop=false;
    flagManagerInEmergency = false;

    //flagNewSubmission=true;
    //flagUpdateSubmissionTime=true;

    return true;
}

bool DroneMissionScheduler::runStart()
{
    if(!DroneModule::run())
        return false;
    else
        droneMissionStateMsg.data = true;
        publishMissionState(droneMissionStateMsg);

        return true;
}

bool DroneMissionScheduler::sendTaskReady()
{
    cout << "" << endl;
    cout << "\033[1;31m....Task Starting\033[0m" << endl;

    //Init task
    taskInitTime=ros::Time::now();
    /*
    if(flagNewSubmission)
    {
        cout<<"....New Submission Started"<<endl;

        //bajamos bandera
        flagNewSubmission=false;

        if(flagUpdateSubmissionTime)
        {
            flagUpdateSubmissionTime=false;

            //cout<<"  .!!updating time!"<<endl;
            submissionInitTime=ros::Time::now();
        }
    }
    */

}

bool DroneMissionScheduler::sendTaskEnded()
{
    cout << "\033[1;31m....Task Finished\033[0m" << endl;

    /// Search for Current Submission ///

    unsigned int iSubm;
    for(iSubm=0;iSubm<MissionScheduler.submissionTree.size();iSubm++)
    {
        if(MissionScheduler.submissionTree.at(iSubm)==submissionCounter)
            break;
    }

    ///Search for Current Task ///

    unsigned int iTask;
    for(iTask=0;iTask<MissionScheduler.taskTree.at(iSubm).size();iTask++)
    {
        if(MissionScheduler.taskTree.at(iSubm).at(iTask)==taskCounter)
            break;
    }


    /// Assign new task to Task Counter ///
    if(iTask<MissionScheduler.taskTree.at(iSubm).size()-1)
    {
        taskCounter=MissionScheduler.taskTree.at(iSubm).at(iTask+1);
    }

    /// If tasks finished in Submission, change Submission and go to initial task. ///
    else
    {
        for(unsigned int iSubmm=0;iSubmm<MissionScheduler.submissionTree.size();iSubmm++)
        {
            if(MissionScheduler.submissionTreeDescription.at(iSubmm)=="FINAL LAND")
            {
                lastSubmission=iSubmm;
            }
        }

        cout << "last submission" << lastSubmission << endl;
        cout << "isubm" << iSubm << endl;

        if(iSubm<lastSubmission) // verificar
        {
            cout << "" << endl;
            cout << "\033[1;33m............NEW SUBMISSION STARTED............\033[0m" << endl;

            submissionCounter=MissionScheduler.submissionTree.at(iSubm+1);
            taskCounter=MissionScheduler.taskTree.at(iSubm+1).at(0);

            submissionInitTime=ros::Time::now();
            taskInitTime=ros::Time::now();
        }
        else
        {
            droneMissionStateMsg.data = false;
            publishMissionState(droneMissionStateMsg);
            /// Mission Finished ///
            cout << "" << endl;
            cout << "\033[1;33m............MISSION FINISHED............\033[0m" << endl;
            return false;
        }
    }

    return true;
}

bool DroneMissionScheduler::readNewTask()
{

    cout<<""<<endl;
    cout << "\033[1;31m....New task Found\033[0m" << endl;
    cout << "\033[1;34m........Current Submission: \033[0m" << "\033[1;32m" << submissionCounter << "\033[0m"<< endl;
    cout << "\033[1;34m........Current Task: \033[0m" << "\033[1;32m" << taskCounter << "\033[0m"<< endl;

    if(!MissionScheduler.readTask(submissionCounter,taskCounter))
    {
        cout<<"****** Submission or Task not found ******"<<endl;
        return false;
    }

    //cout << MissionScheduler.TheTask.speechSent << endl;
    // Publish Speech Message //
    if (MissionScheduler.TheTask.speechSent.size() != 0)
    {
        droneSpeechCommandMsg.data = "Proxima Tarea " + MissionScheduler.TheTask.speechSent;
        droneSpeechCommandPub.publish(droneSpeechCommandMsg);
    }


}

bool DroneMissionScheduler::sendNewTask()
{


    // Time Message //
    droneMissionPlannerCommandMsg.time=ros::Time::now();

    // Command Message //
    switch (MissionScheduler.TheTask.type)
    {
    case missionSchedulerType::hover:
        droneMissionPlannerCommandMsg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::HOVER;
        cmdSent = "HOVER";
        break;
    case missionSchedulerType::land:
        droneMissionPlannerCommandMsg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::LAND;
        cmdSent = "LAND";
        break;
    case missionSchedulerType::takeOff:
        droneManagerStatusMsg = droneMsgsROS::droneManagerStatus::TAKINGOFF;
        droneMissionPlannerCommandMsg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::TAKE_OFF;
        cmdSent = "TAKE_OFF";
        break;
    case missionSchedulerType::sleep:
        cmdSent = "SLEEP";
        break;
    case missionSchedulerType::trajectoryMovement:
        droneMissionPlannerCommandMsg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_TRAJECTORY;
        cmdSent = "MOVE_TRAJ";
        break;
    case missionSchedulerType::emergencyMovement:
        droneMissionPlannerCommandMsg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_EMERGENCY;
        cmdSent = "MOVE_EMERGENCY";
        break;
    case missionSchedulerType::speedMovement:
        droneMissionPlannerCommandMsg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_SPEED;
        cmdSent = "MOVE_SPEED";
        break;
    case missionSchedulerType::positionMovement:
        droneMissionPlannerCommandMsg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_POSITION;
        cmdSent = "MOVE_POS";
        break;
    case missionSchedulerType::VSMovement:
    {
        while(objectOnFrame==false && ros::ok() && !breakMission){

            ROS_INFO("OBJECT TO TRACK NOT RECEIVED");
            ros::spinOnce();
            ros::Duration(0.5).sleep();

        }
        droneMissionPlannerCommandMsg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_VISUAL_SERVOING;
        cmdSent = "MOVE_VS";
        break;
    }
    case missionSchedulerType::flipMovementRight:
        droneMissionPlannerCommandMsg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_RIGHT;
        cmdSent = "MOVE_FLIP_RIGHT";
        break;
    case missionSchedulerType::flipMovementLeft:
        droneMissionPlannerCommandMsg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_LEFT;
        cmdSent = "MOVE_FLIP_LEFT";
        break;
    case missionSchedulerType::flipMovementFront:
        droneMissionPlannerCommandMsg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_FRONT;
        cmdSent = "MOVE_FLIP_FRONT";
        break;
    case missionSchedulerType::flipMovementBack:
        droneMissionPlannerCommandMsg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_BACK;
        cmdSent = "MOVE_FLIP_BACK";
        break;
    default:
        droneMissionPlannerCommandMsg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::UNKNOWN;
        cmdSent = "UNKNOWN";
        break;
    }

   cout << "\033[1;31m....Sending Newest Task: \033[0m" << "\033[1;32m" << cmdSent << "\033[0m"<< endl;

    // Optional Modules Message //
    droneMissionPlannerCommandMsg.drone_modules_names.erase(droneMissionPlannerCommandMsg.drone_modules_names.begin(),droneMissionPlannerCommandMsg.drone_modules_names.end());
    int i;
    if(!MissionScheduler.TheTask.modules.empty()){
        for(i=0; i <= MissionScheduler.TheTask.modules.size()-1; i++)
        {
            droneMissionPlannerCommandMsg.drone_modules_names.push_back(MissionScheduler.TheTask.modules.at(i));
        }
    }

    publishMissionPlannerCommand(droneMissionPlannerCommandMsg);
}


bool DroneMissionScheduler::sendMissionInfo()
{

    /// Set the variable ///
    droneMissionInfoMsg.timeMsgs=ros::Time::now();

    /// Mission ///
    droneMissionInfoMsg.durationMission=ros::Time::now()-startMissionTime-supendedTime;

    /// Submission ///
    droneMissionInfoMsg.durationSubmission=ros::Time::now()-submissionInitTime-supendedTime;
    droneMissionInfoMsg.idSubmission=submissionCounter;
    droneMissionInfoMsg.loopSubmission=MissionScheduler.TheSubmission.loop;

    /// Task ///
    droneMissionInfoMsg.durationTask=ros::Time::now()-taskInitTime-supendedTime;
    droneMissionInfoMsg.idTask=taskCounter;

    switch (MissionScheduler.TheTask.type)
    {
    case missionSchedulerType::hover:
        droneMissionInfoMsg.taskType=droneMsgsROS::droneMissionPlannerCommand::HOVER;
        break;
    case missionSchedulerType::land:
        droneMissionInfoMsg.taskType=droneMsgsROS::droneMissionPlannerCommand::LAND;
        break;
    case missionSchedulerType::takeOff:
        droneMissionInfoMsg.taskType=droneMsgsROS::droneMissionPlannerCommand::TAKE_OFF;
        break;
    case missionSchedulerType::sleep:
        droneMissionInfoMsg.taskType=droneMsgsROS::droneMissionPlannerCommand::SLEEP;
        break;
    case missionSchedulerType::trajectoryMovement:
        droneMissionInfoMsg.taskType=droneMsgsROS::droneMissionPlannerCommand::MOVE_TRAJECTORY;
        break;
    case missionSchedulerType::emergencyMovement:
        droneMissionInfoMsg.taskType=droneMsgsROS::droneMissionPlannerCommand::MOVE_EMERGENCY;
        break;
    case missionSchedulerType::speedMovement:
        droneMissionInfoMsg.taskType=droneMsgsROS::droneMissionPlannerCommand::MOVE_SPEED;
        break;
    case missionSchedulerType::positionMovement:
        droneMissionInfoMsg.taskType=droneMsgsROS::droneMissionPlannerCommand::MOVE_POSITION;
        break;
    case missionSchedulerType::flipMovementRight:
        droneMissionInfoMsg.taskType=droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_RIGHT;
        break;
    case missionSchedulerType::flipMovementLeft:
        droneMissionInfoMsg.taskType=droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_LEFT;
        break;
    case missionSchedulerType::flipMovementFront:
        droneMissionInfoMsg.taskType=droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_FRONT;
        break;
    case missionSchedulerType::flipMovementBack:
        droneMissionInfoMsg.taskType=droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_BACK;
        break;
    case missionSchedulerType::VSMovement:
        droneMissionInfoMsg.taskType=droneMsgsROS::droneMissionPlannerCommand::MOVE_VISUAL_SERVOING;
        break;
    default:
        droneMissionInfoMsg.taskType=droneMsgsROS::droneMissionPlannerCommand::UNKNOWN;
        break;
    }

    if(flagTaskRunning)
        droneMissionInfoMsg.taskState=droneMsgsROS::droneMissionInfo::TASK_RUNNING;
    else
        droneMissionInfoMsg.taskState=droneMsgsROS::droneMissionInfo::WAITING_BRAIN;

    publishMissionInfo(droneMissionInfoMsg);
}


bool DroneMissionScheduler::sendCommandValues()
{
    /// Launch Trajectory Point! ///

    //Point!!!

    bool pointLoaded=false;
    bool speedLoaded=false;


    /// CALCULATE DISTANCE TO OBSTACLES IN MISSION POINT ///
    if(MissionScheduler.TheTask.pointLoaded.at(0) && MissionScheduler.TheTask.pointLoaded.at(1))
    {
        calculateMissionPointDistanceToObstacle(MissionScheduler.TheTask.point);
    }

    /// X ///
    if(MissionScheduler.TheTask.pointLoaded.at(0))
    {
        pointLoaded=true;
        dronePositionRefCommandMsg.x=MissionScheduler.TheTask.point.at(0);
    }
    else
    {
        dronePositionRefCommandMsg.x=droneEstimatedPoseMsg.x;
        MissionScheduler.TheTask.point.at(0)=droneEstimatedPoseMsg.x;
    }

    /// Y ///

    if(MissionScheduler.TheTask.pointLoaded.at(1))
    {
        pointLoaded=true;
        dronePositionRefCommandMsg.y=MissionScheduler.TheTask.point.at(1);
    }
    else
    {
        dronePositionRefCommandMsg.y=droneEstimatedPoseMsg.y;
        MissionScheduler.TheTask.point.at(1)=droneEstimatedPoseMsg.y;

    }

    /// Z ///

    if(MissionScheduler.TheTask.pointLoaded.at(2))
    {
        pointLoaded=true;
        dronePositionRefCommandMsg.z=MissionScheduler.TheTask.point.at(2);
    }
    else
    {
        dronePositionRefCommandMsg.z=droneEstimatedPoseMsg.z;
        MissionScheduler.TheTask.point.at(2)=droneEstimatedPoseMsg.z;
    }

    /// dX ///

    if(MissionScheduler.TheTask.speedLoaded.at(0))
    {

        speedLoaded=true;
        droneSpeedsRefCommandMsg.dx=MissionScheduler.TheTask.speed.at(0)+droneEstimatedSpeedsMsg.dx;
    }
    else
    {
        droneSpeedsRefCommandMsg.dx=droneEstimatedSpeedsMsg.dx;
        MissionScheduler.TheTask.speed.at(0)=droneEstimatedSpeedsMsg.dx;
    }

    /// dY ///

    if(MissionScheduler.TheTask.speedLoaded.at(1))
    {
        speedLoaded=true;
        droneSpeedsRefCommandMsg.dy=MissionScheduler.TheTask.speed.at(1)+droneEstimatedSpeedsMsg.dy;
    }
    else
    {
        droneSpeedsRefCommandMsg.dy=droneEstimatedSpeedsMsg.dy;
        MissionScheduler.TheTask.speed.at(1)=droneEstimatedSpeedsMsg.dy;

    }

    /// dZ ///

    if(MissionScheduler.TheTask.speedLoaded.at(2))
    {
        speedLoaded=true;
        droneSpeedsRefCommandMsg.dz=MissionScheduler.TheTask.speed.at(2)+droneEstimatedSpeedsMsg.dz;
    }
    else
    {
        droneSpeedsRefCommandMsg.dz=droneEstimatedSpeedsMsg.dz;
        MissionScheduler.TheTask.speed.at(2)=droneEstimatedSpeedsMsg.dz;
    }

    /// Publish Point ///
    if(pointLoaded || speedLoaded)
    {
        if(MissionScheduler.TheTask.type==missionSchedulerType::trajectoryMovement || MissionScheduler.TheTask.type==missionSchedulerType::emergencyMovement)
        {
            publishPositionRefCommand(dronePositionRefCommandMsg);
        }
        else if(MissionScheduler.TheTask.type==missionSchedulerType::speedMovement)
        {
            publishSpeedsRefCommand(droneSpeedsRefCommandMsg);
        }
        else if(MissionScheduler.TheTask.type==missionSchedulerType::positionMovement)
        {
            publishPositionRefCommand(dronePositionRefCommandMsg);
        }
    }


    /// YAW ///

    if(MissionScheduler.TheTask.yawLoaded)
    {
        cout << "entra?" << endl;
        //command from xml
        droneYawToLookMsg.header.stamp=ros::Time::now();
        droneYawToLookMsg.yaw=MissionScheduler.TheTask.yaw;
        publishYawToLook(droneYawToLookMsg);
    }

    /// POINT TO LOOK ///

    if(MissionScheduler.TheTask.pointToLookLoaded.at(0) && MissionScheduler.TheTask.pointToLookLoaded.at(1))
    {
        cout << "entra Load?" << endl;
        //comand from xml
        dronePointToLookMsg.x=MissionScheduler.TheTask.pointToLook.at(0);
        dronePointToLookMsg.y=MissionScheduler.TheTask.pointToLook.at(1);
        publishPointToLook(dronePointToLookMsg);
    }
}


bool DroneMissionScheduler::sendMissionMonitor()
{
    //cout << "\033[1;31m....Mission Monitor Online\033[0m" << endl;

    /// MONITOR SUBMISSION DURATION ///
    if(MissionScheduler.TheSubmission.duration!=-1)
    {
        DroneMissionScheduler::calculateSubmissionTimeDuration();

    }

    /// MONITOR BATTERY DURATION ///

    if(MissionScheduler.TheSubmission.battery!=-1)
    {
        DroneMissionScheduler::calculateBatteryDuration();

    }

    /// MONITORING SPECIFIC TASKS ///

    //cout << "\033[1;34m........Task Monitoring Online\033[0m" << endl;

    switch(MissionScheduler.TheTask.type){
    case missionSchedulerType::takeOff:
        //cout << "\033[1;34m........Action: \033[0m" << "\033[1;32mTAKE-OFF (Not Monitored)\033[0m"<< endl;
        if(monitorTakingOff())
            return true;
        break;
    case missionSchedulerType::flipMovementRight:
        //cout << "\033[1;34m........Action: \033[0m" << "\033[1;32mMOVING FLIP (Monitored)\033[0m"<< endl;
        if(monitorFlip())
            return true;
        break;
    case missionSchedulerType::flipMovementLeft:
        //cout << "\033[1;34m........Action: \033[0m" << "\033[1;32mMOVING FLIP (Monitored)\033[0m"<< endl;
        if(monitorFlip())
            return true;
        break;
    case missionSchedulerType::flipMovementFront:
        //cout << "\033[1;34m........Action: \033[0m" << "\033[1;32mMOVING FLIP (Monitored)\033[0m"<< endl;
        if(monitorFlip())
            return true;
        break;
    case missionSchedulerType::flipMovementBack:
        //cout << "\033[1;34m........Action: \033[0m" << "\033[1;32mMOVING FLIP (Monitored)\033[0m"<< endl;
        if(monitorFlip())
            return true;
        break;
    case missionSchedulerType::land:
        cout << "\033[1;34m........Action: \033[0m" << "\033[1;32mLAND (Not Monitored)\033[0m"<< endl;
        return true;
        break;
    case missionSchedulerType::trajectoryMovement:
        //cout << "\033[1;34m........Action: \033[0m" << "\033[1;32mMOVING TRAJECTORY (Monitored)\033[0m"<< endl;
        if (calculatePointDistance())
            return true;
        break;
    case missionSchedulerType::emergencyMovement:
        //cout << "\033[1;34m........Action: \033[0m" << "\033[1;32mMOVING EMERGENCY (Monitored)\033[0m"<< endl;
        if (moveEmergency())
            return true;
        break;
    case missionSchedulerType::positionMovement:
        cout << "\033[1;34m........Action: \033[0m" << "\033[1;32mMOVING POSITION (Monitored)\033[0m"<< endl;
        if (calculatePointDistance())
            return true;
        break;
    case missionSchedulerType::speedMovement:
        cout << "\033[1;34m........Action: \033[0m" << "\033[1;32mMOVING SPEED (Monitored)\033[0m"<< endl;
        if (calculateSpeedDistance())
            return true;
        break;
    case missionSchedulerType::hover:
        //cout << "\033[1;34m........Action: \033[0m" << "\033[1;32mHOVERING (Monitored)\033[0m"<< endl;
        if (calculateTaskTimeDuration())
            return true;
        break;
    case missionSchedulerType::sleep:
        //cout << "\033[1;34m........Action: \033[0m" << "\033[1;32mSLEEPING (Monitored)\033[0m"<< endl;
        if (calculateTaskTimeDuration())
            return true;
        break;
    case missionSchedulerType::VSMovement:
        //cout << "\033[1;34m........Action: \033[0m" << "\033[1;32mMOVING VS (Monitored)\033[0m"<< endl;
        if (monitorVS())
            return true;
        break;

    }


    return false;
}

bool DroneMissionScheduler::calculatePointDistance(){

    /// Position distances! ///
    //Now is a square in x-y. it should be a circle!!
    std::vector<double> distanceToMissionPoint;
    //x
    distanceToMissionPoint.push_back(fabs(droneEstimatedPoseMsg.x-MissionScheduler.TheTask.point.at(0)));
    //y
    distanceToMissionPoint.push_back(fabs(droneEstimatedPoseMsg.y-MissionScheduler.TheTask.point.at(1)));
    //z
    distanceToMissionPoint.push_back(fabs(droneEstimatedPoseMsg.z-MissionScheduler.TheTask.point.at(2)));
    /*

    cout << "" << endl;
    cout << "\033[1;34m........Current Mission Point\033[0m" << endl;
    cout << "\033[1;34m           x:\033[0m" << "\033[1;32m" << MissionScheduler.TheTask.point.at(0) << "\033[0m"<< endl;
    cout << "\033[1;34m           y:\033[0m" << "\033[1;32m" << MissionScheduler.TheTask.point.at(1) << "\033[0m"<< endl;
    cout << "\033[1;34m           z:\033[0m" << "\033[1;32m" << MissionScheduler.TheTask.point.at(2) << "\033[0m"<< endl;
    cout << "\033[1;34m           yaw:\033[0m" << "\033[1;32m" << droneYawRefCommandMsg.yaw          << "\033[0m"<< endl;
    cout << "" << endl;

    cout << "\033[1;34m........Distance to Mission Point\033[0m" << endl;
    cout << "\033[1;34m           ex:\033[0m" << "\033[1;32m" << distanceToMissionPoint.at(0) << "\033[0m"<< endl;
    cout << "\033[1;34m           ey:\033[0m" << "\033[1;32m" << distanceToMissionPoint.at(1) << "\033[0m"<< endl;
    cout << "\033[1;34m           ez:\033[0m" << "\033[1;32m" << distanceToMissionPoint.at(2) << "\033[0m"<< endl;
    */
    /// Yaw! ///


    //yaw distance

    //cout << droneEstimatedPoseMsg.yaw << endl;
    //cout << droneYawRefCommandMsg.yaw << endl;



/*
    cout << "" << endl;
    cout << "\033[1;34m........Distance to Yaw Command\033[0m" << endl;
    cout << "\033[1;34m           eYaw:\033[0m" << "\033[1;32m" << distanceToYaw*180.0/M_PI << "\033[0m"<< endl;
*/

    if(flagDistanceToYaw)
    {

        // atan2: in the interval [-pi,+pi] radians
        distanceToYaw=atan2(sin(droneEstimatedPoseMsg.yaw)*cos(droneYawRefCommandMsg.yaw)-sin(droneYawRefCommandMsg.yaw)*cos(droneEstimatedPoseMsg.yaw),
                            sin(droneEstimatedPoseMsg.yaw)*sin(droneYawRefCommandMsg.yaw)+cos(droneEstimatedPoseMsg.yaw)*cos(droneYawRefCommandMsg.yaw));

        //distanceToYaw=fabs(droneEstimatedPoseMsg.yaw-droneYawRefCommandMsg.yaw);

    }
    //cout << distanceToYaw << endl;

    //Check distances
    bool flagPositionAchieved=false;

    if(distanceToMissionPoint.at(0) <= MissionScheduler.defaultPrecission.at(0) && distanceToMissionPoint.at(1) <= MissionScheduler.defaultPrecission.at(1) && distanceToMissionPoint.at(2) <= MissionScheduler.defaultPrecission.at(2))
    {
        flagPositionAchieved=true;
    }

    //Check yaw distances
    bool flagYawAchieved=false;


    if(flagDistanceToYaw && (distanceToYaw <= MissionScheduler.yawPrecission) )
    {
        flagYawAchieved=true;

        //std::cout<<"Yaw achieved"<<std::endl;
    }


    //distanceToYaw = 100;



    //Finish task
    if(flagPositionAchieved && flagYawAchieved)
    {
        flagDistanceToYaw=false;
        //std::cout<<"Task achieved"<<std::endl;

        return true;
    }

    return false;

}


bool DroneMissionScheduler::calculateSpeedDistance(){

    /// Position distances! ///
    //Now is a square in x-y. it should be a circle!!
    std::vector<double> distanceToMissionSpeeds;

    if(counter == 0)
    {

        initialTargetSpeed.push_back(droneEstimatedSpeedsMsg.dx+MissionScheduler.TheTask.speed.at(0));
        initialTargetSpeed.push_back(droneEstimatedSpeedsMsg.dy+MissionScheduler.TheTask.speed.at(1));
        initialTargetSpeed.push_back(droneEstimatedSpeedsMsg.dz+MissionScheduler.TheTask.speed.at(2));

    }

    counter += 1;
    //x
    distanceToMissionSpeeds.push_back(fabs(droneEstimatedSpeedsMsg.dx-initialTargetSpeed.at(0)));
    //y
    distanceToMissionSpeeds.push_back(fabs(droneEstimatedSpeedsMsg.dy-initialTargetSpeed.at(1)));
    //z
    distanceToMissionSpeeds.push_back(fabs(droneEstimatedSpeedsMsg.dz-initialTargetSpeed.at(2)));

    cout << "->MissionSpeed= dx:" << initialTargetSpeed.at(0) <<
            " dy:" << initialTargetSpeed.at(1) <<
            " dz:" << initialTargetSpeed.at(2) <<
            " dyaw:" << droneYawRefCommandMsg.yaw          << std::endl;
    cout<<"  ->Speed Distances="<<distanceToMissionSpeeds.at(0)<<"; "<<distanceToMissionSpeeds.at(1)<<"; "<<distanceToMissionSpeeds.at(2)<<endl;

    /// Yaw! ///
    //yaw distance
    double distanceToYaw=0.0;

    distanceToYaw=fabs(droneEstimatedPoseMsg.yaw-droneYawRefCommandMsg.yaw);

    cout<<"  ->distance yaw="<<distanceToYaw*180.0/M_PI<<endl;

    //Check distances
    bool flagPositionAchieved=false;

    if(distanceToMissionSpeeds.at(0)<=MissionScheduler.defaultPrecission.at(0) && distanceToMissionSpeeds.at(1)<=MissionScheduler.defaultPrecission.at(1)) //&& distanceToMissionSpeeds.at(2)<=MissionScheduler.defaultPrecission.at(2)//)
    {
        flagPositionAchieved=true;
    }


    //Check yaw distances
    bool flagYawAchieved=false;
    if(distanceToYaw<=MissionScheduler.yawPrecission)
    {
        flagYawAchieved=true;
    }


    //Finish task
    if(flagPositionAchieved && flagYawAchieved)
    {
        initialTargetSpeed.clear();
        counter = 0;
        return true;
    }

    return false;
}


bool DroneMissionScheduler::calculateTaskTimeDuration(){
    //time
    //if(MissionScheduler.TheTask.duration!=-1)
    //{
        //There is a limit in time
        ros::Duration taskDuration;
        taskDuration=ros::Time::now()-taskInitTime-supendedTime;

        //cout << "\033[1;34m........Task Elapsed Time: \033[0m" << "\033[1;32m" << taskDuration.toSec() <<"\033[0m"<< endl;

        if(taskDuration.toSec()>=MissionScheduler.TheTask.duration)
        {
            return true;
        }

        return false;
    //}
}


bool DroneMissionScheduler::calculateSubmissionTimeDuration(){

    cout<<"  .periodic submision"<<endl;

    //There is a limit in time
    ros::Duration submissionDuration;
    submissionDuration=ros::Time::now()-submissionInitTime-supendedTime;

    cout<<"    ->time="<<submissionDuration.toSec()<<" of "<<MissionScheduler.TheSubmission.duration<<endl;

    if(submissionDuration.toSec()>=MissionScheduler.TheSubmission.duration)
    {
        if(MissionScheduler.TheSubmission.loop)
            flagFinishSubmissionLoop=true;
    }

}

bool DroneMissionScheduler::monitorTakingOff()
{
    if (droneManagerStatusMsg!=droneMsgsROS::droneManagerStatus::TAKINGOFF)
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool DroneMissionScheduler::monitorFlip()
{
    //ros::Duration(1.0).sleep();
    //cout << "aqui entra primero?" << endl;


    if (droneManagerStatusMsg==droneMsgsROS::droneManagerStatus::HOVERING)
    {
        //cout << "aqui entra?" << endl;
        resetValues();
        breakMission = true;

        submissionCounter = prevSubmission;
        taskCounter = prevTask;

        DroneMissionScheduler::readNewTask();
        DroneMissionScheduler::sendNewTask();
        return true;

    }
    else
    {
        return false;
    }

}

bool DroneMissionScheduler::moveEmergency()
{
    //ros::Duration(1.0).sleep();
    //cout << "aqui entra primero?" << endl;
    if (droneManagerStatusMsg!=droneMsgsROS::droneManagerStatus::HOVERING)
    {
        //cout << "aqui entra?" << endl;
        resetValues();
        breakMission = true;
        emergencyCountVector=false;

        submissionCounter = prevSubmission;
        taskCounter = prevTask;

        DroneMissionScheduler::readNewTask();
        DroneMissionScheduler::sendNewTask();
        return true;

    }
    else
    {
        return false;
    }
}

bool DroneMissionScheduler::calculateBatteryDuration()
{

}

bool DroneMissionScheduler::monitorVS(){

    /// FINISH VS WITH AN ARUCO ID ///

    for(unsigned int i=0; i<arucoObservations.size();i++)
    {

        if(arucoObservations[i].id == atoi(endVisualServoingArucoId.c_str()))
        {
            return true;
        }
    }

    return false;

    /// FINISH VISUAL_SERVOING WITH DISTANCE FORM A POINT ///
    // Finishes VS when a zone is reached
    /*
    /// Position distances! ///
    //Now is a square in x-y. it should be a circle!!
    std::vector<double> distanceToMissionPoint;
    //x
    distanceToMissionPoint.push_back(fabs(droneEstimatedPoseMsg.x-MissionScheduler.TheTask.point.at(0)));
    //y
    distanceToMissionPoint.push_back(fabs(droneEstimatedPoseMsg.y-MissionScheduler.TheTask.point.at(1)));
    //z
    distanceToMissionPoint.push_back(fabs(droneEstimatedPoseMsg.z-MissionScheduler.TheTask.point.at(2)));

    //droneObs

    cout << "" << endl;
    cout << "\033[1;34m........Current Mission Point\033[0m" << endl;
    cout << "\033[1;34m           x:\033[0m" << "\033[1;32m" << MissionScheduler.TheTask.point.at(0) << "\033[0m"<< endl;
    cout << "\033[1;34m           y:\033[0m" << "\033[1;32m" << MissionScheduler.TheTask.point.at(1) << "\033[0m"<< endl;
    cout << "\033[1;34m           z:\033[0m" << "\033[1;32m" << MissionScheduler.TheTask.point.at(2) << "\033[0m"<< endl;
    cout << "\033[1;34m           yaw:\033[0m" << "\033[1;32m" << droneYawRefCommandMsg.yaw          << "\033[0m"<< endl;
    cout << "" << endl;

    cout << "\033[1;34m........Distance to Mission Point\033[0m" << endl;
    cout << "\033[1;34m           ex:\033[0m" << "\033[1;32m" << distanceToMissionPoint.at(0) << "\033[0m"<< endl;
    cout << "\033[1;34m           ey:\033[0m" << "\033[1;32m" << distanceToMissionPoint.at(1) << "\033[0m"<< endl;
    cout << "\033[1;34m           ez:\033[0m" << "\033[1;32m" << distanceToMissionPoint.at(2) << "\033[0m"<< endl;

    //Check distances

    if(distanceToMissionPoint.at(0) <= MissionScheduler.defaultPrecission.at(0) && distanceToMissionPoint.at(1) <= MissionScheduler.defaultPrecission.at(1) && distanceToMissionPoint.at(2) <= MissionScheduler.defaultPrecission.at(2))
    {
        return true;
    }

    return false;
    */

}

bool DroneMissionScheduler::calculateMissionPointDistanceToObstacle(std::vector<double> missionPointIn)
{

    bool missionPointCorrect = false;
    while (missionPointCorrect == false)
    {

        for ( std::vector<droneMsgsROS::obstacleTwoDimPole>::const_iterator it = obstaclesMsg.poles.begin();
              it != obstaclesMsg.poles.end();
              ++it) {


            h = it->centerX;
            k = it->centerY;
            rx = it->radiusX;
            ry = it->radiusY;

            if ((pow((missionPointIn.at(0) - h),2)/pow(rx*2,2)) + (pow((missionPointIn.at(1) - k),2)/pow(ry*2,2)) <= 1)
            {
                cout << "\033[1;34m....CURRENT MISSION POINT TOO CLOSE TO OBSTACLE....\033[0m" << endl;

                MissionScheduler.TheTask.point.at(0) = MissionScheduler.TheTask.point.at(0);
                MissionScheduler.TheTask.point.at(1) = MissionScheduler.TheTask.point.at(1) + 8*ry;

                cout << MissionScheduler.TheTask.point.at(0) << endl;
                cout << MissionScheduler.TheTask.point.at(1) << endl;
                missionPointIn.at(0) = missionPointIn.at(0);
                missionPointIn.at(1) = missionPointIn.at(1) + 8*ry;
                missionPointCorrect = false;
                break;
            }

            missionPointCorrect = true;
        }
    }
}




