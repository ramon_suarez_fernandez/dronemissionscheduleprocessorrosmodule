#include "ros/ros.h"
#include <stdio.h>
#include <iostream>

#include "droneMissionScheduleProcessorROSModule.h"

using namespace std;

int main(int argc, char **argv)
{

    ros::init(argc, argv, MODULE_NAME_MISSION_PLANNER);
  	ros::NodeHandle n;

    cout<<"Starting drone Mission Scheduler..."<<endl;

    DroneMissionScheduler MyDroneMissionScheduler;
    MyDroneMissionScheduler.open(n,MODULE_NAME_MISSION_PLANNER);

    //while(ros::ok())
    //{

    /// START ///
    ros::spin();

    //}

    return 1;

}    
